﻿using Microsoft.EntityFrameworkCore;
using Redjar.MyApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Redjar.MyApp.Domain.Repositories
{
	public class MyAppDbContext : DbContext
	{
		public MyAppDbContext(DbContextOptions options) : base(options)
		{

		}

		DbSet<Person> People { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}
}
