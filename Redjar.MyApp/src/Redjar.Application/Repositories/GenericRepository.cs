﻿using Microsoft.EntityFrameworkCore;
using Redjar.MyApp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Redjar.Application.Repositories
{
	public class GenericRepository<T> : IGenericRepository<T> where T : class
	{
		private readonly MyAppDbContext _myAppDbContext;

		public GenericRepository(MyAppDbContext myAppDbContext)
		{
			_myAppDbContext = myAppDbContext;
		}

		public void Delete(T entity)
		{
			_myAppDbContext.Entry(entity).State = EntityState.Deleted;
		}

		public void Delete(Guid id)
		{
			var entity = _myAppDbContext.Set<T>().Find(id);

			_myAppDbContext.Entry(entity).State = EntityState.Deleted;
		}

		public T Find(Guid Id)
		{
			return _myAppDbContext.Set<T>().Find(Id);
			//To do: Desactivar Tracking de la entidad.
		}

		public void Insert(T entity)
		{
			_myAppDbContext.Set<T>().Add(entity);
		}

		public IQueryable<T> List(Expression<Func<T, bool>> expression)
		{ 
			return _myAppDbContext.Set<T>().AsNoTracking().Where(expression);
		}

		public void Patch(T entity, Guid id)
		{
			throw new NotImplementedException(); // TO DO
		}

		public void Update(T entity)
		{
			_myAppDbContext.Entry(entity).State = EntityState.Modified; // EntityState.Modified no hacer recursiva la modificacion
		}
	}
}
